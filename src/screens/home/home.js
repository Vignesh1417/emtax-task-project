import { Button, Grid } from "@mui/material";
import React, { useEffect } from "react";
import SelectLabels from "../../components/dropDown";
import { Layout } from "../../components/navbars/topNavbar/topNavbar";
import FullWidthTabs from "../../components/tabs";
import useApiStore from "../../store/emtax";
import { country, entity, filing } from "../../utils/dummyDatas";
import { useStyles } from "./style";

export const Home = (props) => {
  const classes = useStyles();

  const {
    isLoading,
    fetchAll,
    chartData,
    customerData,
    productData,
    invoiceData,
    isAuthenticated,
    handleLogout,
    handleLogin,
  } = useApiStore();

  const [entityValue, setEntityValue] = React.useState("");
  const [countryValue, setCountryValue] = React.useState("");
  const [filingValue, setFilingValue] = React.useState("");

  const logout = () => {
    setEntityValue("");
    setCountryValue("");
    setFilingValue("");
    handleLogout();
  };

  useEffect(() => {
    if (entityValue !== "" && countryValue !== "" && filingValue !== "") {
      fetchAll(entityValue, countryValue, filingValue);
    }
  }, [entityValue, countryValue, filingValue, fetchAll]);

  return (
    <div style={{ width: "100%", height: "100vh" }}>
      <Layout isAuthenticated={isAuthenticated} onLogout={logout}>
        <div className={classes.root}>
          <Grid container gap={3} justifyContent="center" mt={2.5}>
            <Grid item xs={12} sm={6} md={6} lg={3}>
              <SelectLabels
                title="Choose Entity"
                options={entity}
                value={entityValue}
                handleChange={(event) => setEntityValue(event.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={2}>
              <SelectLabels
                title="Select Country"
                options={country}
                value={countryValue}
                handleChange={(event) => setCountryValue(event.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={3}>
              <SelectLabels
                title="Choose Filing"
                options={filing}
                value={filingValue}
                handleChange={(event) => setFilingValue(event.target.value)}
              />
            </Grid>
          </Grid>
          <div className={classes.tabPanel}>
            <FullWidthTabs
              loading={isLoading}
              data={chartData}
              customerData={customerData}
              productData={productData}
              invoiceData={invoiceData}
            />
          </div>
        </div>
      </Layout>

      {!isAuthenticated && (
        <div className={classes.button}>
          <Button onClick={handleLogin}>Login</Button>
        </div>
      )}
    </div>
  );
};
