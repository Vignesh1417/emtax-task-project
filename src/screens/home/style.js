import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormControl-root": {
      width: "100%",
      "& .MuiSelect-select": {
        padding: "8.5px 14px",
        borderRadius: "0px",
      },
      "& fieldset": {
        borderRadius: "0px",
        borderColor: theme.palette.border.main,
      },
    },
  },

  tabPanel: {
    marginTop: "50px",
  },

  button: {
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& button": {
      backgroundColor: theme.palette.background.purple,
      color: theme.palette.background.white,
      "&:hover": {
        backgroundColor: theme.palette.background.purple,
        color: theme.palette.background.white,
      },
    },
  },
}));
