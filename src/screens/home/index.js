import React from "react";
import { Home } from "./home";

class HomeParent extends React.Component {
  render() {
    return <Home />;
  }
}

export default HomeParent;
