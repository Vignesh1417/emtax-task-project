import { Box } from "@mui/material";
import React from "react";
import {
  BarChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Bar,
} from "recharts";
import { BarChartStyles } from "./style";

function BarChartComponent(props) {
  const { chartData = [] } = props;
  return (
    <Box sx={BarChartStyles.barRoot}>
      <BarChart width={390} height={369} data={chartData}>
        <CartesianGrid />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="value" barSize={30} />
      </BarChart>
    </Box>
  );
}

export default BarChartComponent;
