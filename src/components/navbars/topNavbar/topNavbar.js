import { Menu as MenuIcon } from "@mui/icons-material";
import { AppBar, IconButton, Toolbar, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import EmtaxImage from "../../../assets/emtax_logo.png";
import { LogoutIcon } from "../../../utils";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
    zIndex: theme.zIndex.drawer + 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    background:
      "linear-gradient(90deg, #485294 0%, rgba(213, 38, 41, 0.58) 100%), #6E8CD7",
    "& .MuiToolbar-root": {
      justifyContent: "space-between",
    },
  },
  title: {
    display: "block",
  },
  titleContainer: {
    marginLeft: theme.spacing(2),
  },
  menuIcon: {
    [theme.breakpoints.down("lg")]: {
      display: "none",
    },
  },
  signOut: {
    "& p": {
      fontSize: "13px",
      fontStyle: "normal",
      fontWeight: 400,
      marginLeft: "5px",
    },
  },
}));

export const TopNavBar = ({ onLogout }) => {
  const classes = useStyles();

  const toogleSideNavBar = () => {
    console.log("sideBar");
  };

  return (
    <div className={classes.grow}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <div style={{ display: "flex", alignItems: "center" }}>
            <IconButton
              className={classes.menuIcon}
              onClick={toogleSideNavBar}
              size="large"
            >
              <MenuIcon htmlColor="white" />
            </IconButton>

            <div className={classes.titleContainer}>
              <img src={EmtaxImage} alt="Logo" />
            </div>
          </div>

          <div className={classes.centerText}>
            <Typography>Dashboard</Typography>
          </div>

          <div className={classes.signOut}>
            <IconButton
              aria-label="logout button"
              aria-controls={"logout_button"}
              aria-haspopup="true"
              onClick={onLogout}
              color="inherit"
              size="large"
            >
              <LogoutIcon />
              <Typography>Sign Out</Typography>
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export const Layout = ({ children, isAuthenticated, onLogout }) => {
  return (
    <div>
      {isAuthenticated && (
        <>
          <TopNavBar onLogout={onLogout} />
          <div>{children}</div>
        </>
      )}
    </div>
  );
};
