import { Box, Typography } from "@mui/material";
import React from "react";
import loader from "../../assets/ball_loader.png";

export const NewLoader = ({ minusHeight = "0px" }) => {
  const style = {
    box: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      background: "#fff",
      height: "369px",
      flexDirection: "column",
      "& img": {
        width: "62px",
        height: "30px",
      },
    },

    text: {
      color: "#485294",
      fontSize: "13px",
      fontWeight: 400,
    },
  };

  return (
    <>
      <Box sx={style.box}>
        <img src={loader} alt="" />
        <Typography sx={style.text}>Fetching Reports</Typography>
      </Box>
    </>
  );
};
