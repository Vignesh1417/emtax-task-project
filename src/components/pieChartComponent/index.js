import React from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Pie } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);

export function PieChartComponent({ chartData = [] }) {
  return (
    <Pie
      data={{
        labels: chartData?.map((item) => item.name),
        datasets: [
          {
            data: chartData?.map((item) => item.value),
            backgroundColor: [
              "#0088FE",
              "#00C49F",
              "#555D50",
              "#FF8042",
              "#000000",
              "#FF0000",
              "#FFBB28",
              "#32174D",
              "#FFFF00",
            ],
          },
        ],
      }}
    />
  );
}
