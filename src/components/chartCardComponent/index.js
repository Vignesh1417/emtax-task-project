import { Box, Grid, Typography } from "@mui/material";
import * as React from "react";
import { chartCardStyles } from "./style";
import DownloadRoundedIcon from "@mui/icons-material/DownloadRounded";
import BarChartComponent from "../barChartComponent";
import HorizontalBarChartComponent from "../horizontalBarChart";
import Loader from "../../utils/loader";
import { PieChartComponent } from "../pieChartComponent";
import MuiTable from "../table";

export default function ChartCard(props) {
  const classes = chartCardStyles();

  const {
    titleOne = "",
    titleTwo = "",
    titleThree = "",
    data = [],
    loading = false,
    tableTitleOne = "",
    tableTitleTwo = "",
    tableTitleThree = "",
    invoiceData = [],
    customerData = [],
    productData = [],
  } = props;

  const pieChartColors = [
    "#0088FE",
    "#00C49F",
    "#555D50",
    "#FF8042",
    "#000000",
    "#FF0000",
    "#FFBB28",
    "#32174D",
    "#FFFF00",
  ];

  return (
    <Box className={classes.root}>
      {/* Chart */}

      <Box className={classes.chartCard}>
        <Grid
          container
          mt={3}
          mb={3}
          marginX={3}
          gap={2}
          sx={{ width: "initial !important", justifyContent: "center" }}
        >
          <Grid item xs={12} sm={12} md={3.8} lg={3.8}>
            <Box className={classes.card}>
              <Box className={classes.topSection}>
                <Typography className={classes.title}>{titleOne}</Typography>
                <DownloadRoundedIcon />
              </Box>
              {loading ? (
                <Box
                  sx={{
                    width: "100%",
                    height: "369px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader />
                  <Typography className={classes.text}>
                    Fetching Reports
                  </Typography>
                </Box>
              ) : (
                <Box sx={{ height: "369px" }}>
                  <BarChartComponent chartData={data} />
                </Box>
              )}
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={3.8} lg={3.8}>
            <Box className={classes.card}>
              <Box className={classes.topSection}>
                <Typography className={classes.title}>{titleTwo}</Typography>
                <DownloadRoundedIcon />
              </Box>
              {loading ? (
                <Box
                  sx={{
                    width: "100%",
                    height: "369px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader />
                  <Typography className={classes.text}>
                    Fetching Reports
                  </Typography>
                </Box>
              ) : (
                <Box>
                  <PieChartComponent
                    chartData={data}
                    chartColors={pieChartColors}
                  />
                </Box>
              )}
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={3.8} lg={3.8}>
            <Box className={classes.card}>
              <Box className={classes.topSection}>
                <Typography className={classes.title}>{titleThree}</Typography>
                <DownloadRoundedIcon />
              </Box>
              {loading ? (
                <Box
                  sx={{
                    width: "100%",
                    height: "369px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader />
                  <Typography className={classes.text}>
                    Fetching Reports
                  </Typography>
                </Box>
              ) : (
                <Box>
                  <HorizontalBarChartComponent chartData={data} />
                </Box>
              )}
            </Box>
          </Grid>
        </Grid>
      </Box>

      {/* Table */}

      <Box className={classes.tableCard}>
        <Grid
          container
          mt={3}
          mb={3}
          marginX={3}
          gap={2}
          sx={{ width: "initial !important", justifyContent: "center" }}
        >
          <Grid item xs={12} sm={12} md={3.8} lg={3.8}>
            <Box className={classes.card}>
              <Box className={classes.topSection}>
                <Typography className={classes.title}>
                  {tableTitleOne}
                </Typography>
                <DownloadRoundedIcon />
              </Box>
              {loading ? (
                <Box
                  sx={{
                    width: "100%",
                    height: "369px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader />
                  <Typography className={classes.text}>
                    Fetching Reports
                  </Typography>
                </Box>
              ) : (
                <Box sx={{ height: "397px" }}>
                  <MuiTable
                    columns={["Item", "Sum", "ItemDesc"]}
                    data={productData}
                  />
                </Box>
              )}
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={3.8} lg={3.8}>
            <Box className={classes.card}>
              <Box className={classes.topSection}>
                <Typography className={classes.title}>
                  {tableTitleTwo}
                </Typography>
                <DownloadRoundedIcon />
              </Box>
              {loading ? (
                <Box
                  sx={{
                    width: "100%",
                    height: "369px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader />
                  <Typography className={classes.text}>
                    Fetching Reports
                  </Typography>
                </Box>
              ) : (
                <Box sx={{ height: "397px" }}>
                  <MuiTable columns={["Customer", "Sum"]} data={customerData} />
                </Box>
              )}
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={3.8} lg={3.8}>
            <Box className={classes.card}>
              <Box className={classes.topSection}>
                <Typography className={classes.title}>
                  {tableTitleThree}
                </Typography>
                <DownloadRoundedIcon />
              </Box>
              {loading ? (
                <Box
                  sx={{
                    width: "100%",
                    height: "369px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader />
                  <Typography className={classes.text}>
                    Fetching Reports
                  </Typography>
                </Box>
              ) : (
                <Box sx={{ height: "397px" }}>
                  <MuiTable
                    columns={["Customer", "InvoiceNo", "Sum"]}
                    data={invoiceData}
                  />
                </Box>
              )}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
