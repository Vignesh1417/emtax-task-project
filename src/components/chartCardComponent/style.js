import { makeStyles } from "@mui/styles";

export const chartCardStyles = makeStyles((theme) => ({
  root: {
    width: "initial",
  },

  card: {
    backgroundColor: theme.palette.background.white,
    boxShadow:
      " 0px 2px 4px -1px rgba(0, 0, 0, 0.06), 0px 4px 6px -1px rgba(0, 0, 0, 0.10)",
    height: "491px",
    minHeight: "400px",
  },

  topSection: {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: theme.palette.background.main,
    padding: "8px 25px",
  },

  title: {
    fontSize: "14px",
    color: theme.palette.text.black,
    fontWeight: 800,
  },

  text: {
    color: theme.palette.text.main,
    fontSize: "13px",
    fontWeight: 400,
  },

  tableCard: {
    "& .MuiPaper-elevation1": {
      width: "100%",
      height: "294px !important",
    },
    "& .MuiPaper-elevation": {
      backgroundColor: theme.palette.background.white,
      color: theme.palette.text.black,
      boxShadow: "none",
      borderRadius: "2px 0px 0px 2px",
      border: "1px solid",
      borderColor: theme.palette.border.grey,
      "& .MuiTabs-root": {
        minHeight: "40px",
      },
      "& .MuiTabs-indicator": {
        backgroundColor: "transparent",
      },
    },

    "& th": {
      fontWeight: 700,
      color: "rgb(100 95 95 / 87%)",
    },
  },

  chartCard: {
    "& .MuiPaper-elevation": {
      width: "50%",
      backgroundColor: theme.palette.background.white,
      color: theme.palette.text.black,
      boxShadow: "none",
      borderRadius: "2px 0px 0px 2px",
      border: "1px solid",
      borderColor: theme.palette.border.grey,
      height: "40px",
      "& .MuiTabs-root": {
        minHeight: "40px",
      },
      "& .MuiTabs-indicator": {
        backgroundColor: "transparent",
      },
    },
  },
}));
