import * as React from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { useTheme } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { tabsStyles } from "./style";
import ChartCard from "../chartCardComponent";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

export default function FullWidthTabs(props) {
  const classes = tabsStyles();

  const {
    data = [],
    loading = false,
    invoiceData = [],
    customerData = [],
    productData = [],
  } = props;

  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <Box className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          textColor="inherit"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Sales" {...a11yProps(0)} />
          <Tab label="Purchase" {...a11yProps(1)} />
          <Tab label="VAT Analysis" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <Box sx={{ width: "100%" }}>
        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          <ChartCard
            titleOne="SALES & VAT TREND ANALYSIS"
            titleTwo="VAT TREATMENT BREAKUP"
            titleThree="SALES ANALYSIS BY CUSTOMER TYPE"
            tableTitleOne="TOP LIST BY PRODUCT"
            tableTitleTwo="TOP LIST BY CUSTOMER"
            tableTitleThree="TOP LIST BY TAX INVOICE VALUE"
            index={0}
            dir={theme.direction}
            data={data}
            loading={loading}
            invoiceData={invoiceData}
            customerData={customerData}
            productData={productData}
          />
          <ChartCard
            titleOne="SALES & VAT TREND ANALYSIS"
            titleTwo="VAT TREATMENT BREAKUP"
            titleThree="SALES ANALYSIS BY CUSTOMER TYPE"
            tableTitleOne="TOP LIST BY PRODUCT"
            tableTitleTwo="TOP LIST BY CUSTOMER"
            tableTitleThree="TOP LIST BY TAX INVOICE VALUE"
            index={1}
            dir={theme.direction}
            data={data}
            loading={loading}
            invoiceData={invoiceData}
            customerData={customerData}
            productData={productData}
          />
          <ChartCard
            titleOne="SALES & VAT TREND ANALYSIS"
            titleTwo="VAT TREATMENT BREAKUP"
            titleThree="SALES ANALYSIS BY CUSTOMER TYPE"
            tableTitleOne="TOP LIST BY PRODUCT"
            tableTitleTwo="TOP LIST BY CUSTOMER"
            tableTitleThree="TOP LIST BY TAX INVOICE VALUE"
            index={2}
            dir={theme.direction}
            data={data}
            loading={loading}
            invoiceData={invoiceData}
            customerData={customerData}
            productData={productData}
          />
        </SwipeableViews>
      </Box>
    </Box>
  );
}
