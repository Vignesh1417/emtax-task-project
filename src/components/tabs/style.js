import { makeStyles } from "@mui/styles";

export const tabsStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    "& .MuiPaper-elevation": {
      width: "50%",
      backgroundColor: theme.palette.background.white,
      color: theme.palette.text.black,
      boxShadow: "none",
      borderRadius: "2px 0px 0px 2px",
      border: "1px solid",
      borderColor: theme.palette.border.grey,
      height: "40px",
      "& .MuiTabs-root": {
        minHeight: "40px",
      },
      "& .MuiTabs-indicator": {
        backgroundColor: "transparent",
      },
    },
    "& .MuiTableContainer-root": {
      width: "100%",
    },
    "& .MuiButtonBase-root": {
      minHeight: "40px",
      borderRight: "1.5px solid",
      borderColor: theme.palette.border.grey,
      textTransform: "capitalize",
      color: theme.palette.text.black,
      opacity: 1,
      "&:last-child": {
        borderRight: "0px",
      },
    },
    "& .Mui-selected": {
      backgroundColor: theme.palette.background.purple,
      color: theme.palette.background.white,
    },
  },
}));
