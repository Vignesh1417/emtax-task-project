import { Typography } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import * as React from "react";
import { selectLabelsStyles } from "./style";

export default function SelectLabels(props) {
  const classes = selectLabelsStyles();
  const {
    title = "",
    options = [],
    value = "",
    handleChange = () => false,
  } = props;

  return (
    <div>
      <Typography className={classes.title}>{title}</Typography>
      <FormControl sx={{ m: 1, minWidth: 120 }}>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={value}
          onChange={handleChange}
        >
          {options?.map((val) => {
            return <MenuItem value={val?.value}>{val?.label}</MenuItem>;
          })}
        </Select>
      </FormControl>
    </div>
  );
}
