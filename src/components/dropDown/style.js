import { makeStyles } from "@mui/styles";

export const selectLabelsStyles = makeStyles((theme) => ({
  title: {
    fontSize: "13px",
    color: theme.palette.text.main,
    fontWeight: 700,
    textAlign: "center",
  },
}));
