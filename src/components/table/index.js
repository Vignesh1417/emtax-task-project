import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Box,
  Button,
  Typography,
  TablePagination,
  TextField,
  InputAdornment,
} from "@mui/material";
import { tableStyles } from "./style";
import { useState } from "react";
import SearchIcon from "@mui/icons-material/Search";

const MuiTable = (props) => {
  const classes = tableStyles();

  const { data = [], columns = [] } = props;

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [filterValues, setFilterValues] = useState(columns?.map(() => ""));

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const applyFilters = () => {
    return data?.filter((row) =>
      columns?.every((column, columnIndex) => {
        const filterValue = filterValues[columnIndex]?.toLowerCase();
        const cellValue = String(row[column])?.toLowerCase();
        return cellValue?.includes(filterValue);
      })
    );
  };

  const slicedData = applyFilters()?.slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage
  );

  const handleFilterChange = (event, columnIndex) => {
    const newFilterValues = [...filterValues];
    newFilterValues[columnIndex] = event.target.value;
    setFilterValues(newFilterValues);
    setPage(0); // Reset to the first page when a filter changes
  };

  const CustomPaginationActions = () => {
    const totalPages = Math.ceil(applyFilters()?.length / rowsPerPage);
    return (
      <Box className={classes.customPagination}>
        <Button
          className={classes.left}
          onClick={() => handleChangePage(null, page - 1)}
          disabled={page === 0}
        >
          Previous
        </Button>
        {Array.from({ length: totalPages }, (_, index) => (
          <Button
            key={index}
            className={
              page === index ? classes.pageNumber : classes.pageNumberNext
            }
            onClick={() => handleChangePage(null, index)}
          >
            {index + 1}
          </Button>
        ))}
        <Button
          onClick={() => handleChangePage(null, page + 1)}
          disabled={page >= Math.ceil(applyFilters()?.length / rowsPerPage) - 1}
        >
          Next
        </Button>
      </Box>
    );
  };

  return (
    <Box className={classes.table}>
      <Box className={classes.topPagination}>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data?.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          labelRowsPerPage="Show Entries"
        />
      </Box>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow className={classes.headerSection}>
              {columns?.map((column, columnIndex) => (
                <TableCell key={columnIndex}>{column}</TableCell>
              ))}
            </TableRow>
            <TableRow className={classes.textfieldSection}>
              {columns?.map((column, columnIndex) => (
                <TableCell key={columnIndex}>
                  <TextField
                    variant="outlined"
                    size="small"
                    fullWidth
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SearchIcon />
                        </InputAdornment>
                      ),
                    }}
                    value={filterValues[columnIndex]}
                    onChange={(event) => handleFilterChange(event, columnIndex)}
                  />
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {slicedData?.map((row, rowIndex) => (
              <TableRow key={rowIndex}>
                {columns?.map((column, columnIndex) => (
                  <TableCell key={columnIndex}>
                    {typeof row[column] === "number"
                      ? row[column]?.toFixed(2)
                      : row[column]}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Typography className={classes.text}>{`Showing ${
        page * rowsPerPage + 1
      } to ${
        (page + 1) * rowsPerPage > data?.length
          ? data?.length
          : (page + 1) * rowsPerPage
      } of ${data?.length} entries`}</Typography>
      <Box className={classes.pagination}>
        {applyFilters()?.length > 0 && (
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={applyFilters()?.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={CustomPaginationActions}
          />
        )}
      </Box>
    </Box>
  );
};

export default MuiTable;
