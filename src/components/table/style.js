import { makeStyles } from "@mui/styles";

export const tableStyles = makeStyles((theme) => ({
  table: {
    padding: "12px",
    "& tbody": {
      "& tr": {
        "&:nth-of-type(even)": {
          backgroundColor: theme.palette.background.secondary,
        },
        "& td": {
          fontSize: "12px",
          padding: "12px",
          borderBottom: "1px solid lightgray",
        },
      },
    },
    "& thead": {
      "& th": {
        fontSize: "12px",
        padding: "8px",
        borderBottom: "1.5px solid lightgray",
        "& .MuiInputAdornment-root": {
          width: "initial",
        },
      },
    },
    "& .MuiOutlinedInput-root": {
      backgroundColor: theme.palette.background.secondary,
      borderRadius: "0px",
      height: "35px",
      "& fieldset": {
        borderWidth: "0px",
      },
    },
    "& .MuiButton-root": {
      minWidth: "25px",
    },
    "& .MuiButtonBase-root": {
      borderRight: "1.5px solid",
      borderColor: theme.palette.border.grey,
      textTransform: "capitalize",
      color: theme.palette.text.black,
      opacity: 1,
      "&:last-child": {
        borderRight: "0px",
      },
    },
  },

  headerSection: {
    backgroundColor: theme.palette.background.main,
  },

  textfieldSection: {
    backgroundColor: theme.palette.background.white,
  },

  text: {
    fontSize: "12px",
    fontWeight: 600,
    mt: 1.5,
  },

  pagination: {
    "& .MuiBox-root": {
      width: "initial",
    },
    "& .MuiTablePagination-selectLabel": {
      display: "none",
    },
    "& .MuiInputBase-root": {
      display: "none",
    },
    "& .MuiToolbar-root": {
      justifyContent: "right",
      paddingRight: "0",
    },
    "& .MuiTablePagination-actions": {
      width: "30%",
    },
    "& .MuiTablePagination-displayedRows": {
      display: "none",
    },
  },

  topPagination: {
    "& .MuiTablePagination-select": {
      border: "1px solid",
      borderColor: theme.palette.border.grey,
      width: "25px",
    },
    "& .MuiTablePagination-displayedRows": {
      display: "none",
    },
    "& .MuiTablePagination-actions": {
      display: "none",
    },
    "& .MuiToolbar-root": {
      justifyContent: "left",
      width: "22%",
      paddingLeft: "0",
    },
    "& .MuiInputBase-colorPrimary": {
      width: "50px",
    },
  },

  "& .MuiTablePagination-actions": {
    width: "30%",
  },

  customPagination: {
    border: "1px solid",
    borderColor: theme.palette.border.grey,
    display: "flex",
    padding: "0px 15px",
    paddingRight: "0px",
    borderRadius: "4px",
    "& .MuiButton-root": {
      borderRadius: "0",
    },
  },

  pageNumber: {
    borderRight: "1px solid",
    borderColor: theme.palette.border.grey,
    backgroundColor: theme.palette.background.purple,
    padding: "0px 10px",
    pl: "15px",
    color: `${theme.palette.background.white} !important`,
    "&:hover": {
      backgroundColor: theme.palette.background.purple,
      color: `${theme.palette.background.white} !important`,
    },
  },

  pageNumberNext: {
    borderRight: "1px solid",
    borderColor: theme.palette.border.grey,
    padding: "0px 10px",
    pl: "15px",
  },

  left: {
    paddingRight: "15px",
  },
}));
