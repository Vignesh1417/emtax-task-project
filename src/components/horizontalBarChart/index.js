import { Box } from "@mui/material";
import React from "react";
import {
  BarChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Bar,
} from "recharts";
import { HorizontalBarChartStyles } from "./style";

function HorizontalBarChartComponent(props) {
  const classes = HorizontalBarChartStyles();

  const { chartData = [] } = props;

  return (
    <Box className={classes.barRoot}>
      <BarChart width={390} height={369} data={chartData} layout="vertical">
        <CartesianGrid />
        <XAxis type="number" />
        <YAxis type="category" />
        <Tooltip />
        <Legend />
        <Bar dataKey="value" fill="rgba(75, 192, 192, 0.6)" />
      </BarChart>
    </Box>
  );
}

export default HorizontalBarChartComponent;
