import { makeStyles } from "@mui/styles";

export const HorizontalBarChartStyles = makeStyles((theme) => ({
  barRoot: {
    "& .recharts-xAxis": {
      "& tspan": {
        fontSize: "13px",
      },
    },
    "& .recharts-yAxis": {
      "& tspan": {
        fontSize: "13px",
      },
    },
  },
}));
