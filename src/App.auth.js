import { LinearProgress } from "@mui/material";
import React from "react";
// import { useRefreshQuery } from "./redux/services/auth";

export const AppAuth = (props) => {
  // const { isLoading } = useRefreshQuery();
  const isLoading = false;

  return (
    <>
      {isLoading && <LinearProgress />}
      {props.children}
    </>
  );
};
