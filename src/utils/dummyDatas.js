export const entity = [
  {
    label: "ABC LTD",
    value: 202,
  },
  {
    label: "DEF LTD",
    value: 203,
  },
  {
    label: "GHI LTD",
    value: 204,
  },
  {
    label: "JKL LTD",
    value: 205,
  },
  {
    label: "XYZ LTD",
    value: 201,
  },
];

export const country = [
  {
    label: "India",
    value: 1,
  },
  {
    label: "Australia",
    value: 2,
  },
  {
    label: "England",
    value: 3,
  },
  {
    label: "Sri Lanka",
    value: 4,
  },
  {
    label: "UAE",
    value: 5,
  },
];

export const filing = [
  {
    label: "ALL",
    value: 0,
  },
  {
    label: "IT filing",
    value: 1,
  },
  {
    label: "Non-IT Filing",
    value: 2,
  },
  {
    label: "Filing",
    value: 3,
  },
  {
    label: "Income Tax",
    value: 4,
  },
];
