import * as React from "react";

function Loader(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={63}
      height={63}
      viewBox="0 0 63 63"
      fill="none"
      {...props}
    >
      <path d="M20.587 18.928h-9.35v25.034h9.35V18.928z" fill="#4658AC" />
      <path d="M36.137 18.66h-9.35v25.57h9.35V18.66z" fill="#E7008A" />
      <path d="M51.686 18.376h-9.35v26.138h9.35V18.376z" fill="#FF003A" />
    </svg>
  );
}

export default Loader;
