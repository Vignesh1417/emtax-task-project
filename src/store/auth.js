import { create } from "zustand";

const useAuthStore = create((set) => ({
  token: null,

  setAuthToken: (newToken) => {
    console.log("Token : ", newToken);
    set({ token: newToken });
  },
}));

export default useAuthStore;
