import { create } from "zustand";
import { getHeaders } from "../utils/constants";

const useApiStore = create((set, get) => ({
  chartData: null,
  invoiceData: null,
  productData: null,
  customerData: null,
  isLoading: true,
  isAuthenticated: true,

  fetchCustomerData: async (entityValue, countryValue, filingValue) => {
    try {
      const requestData = {
        entity_id: entityValue,
        country_id: countryValue,
        filing_id: filingValue,
      };

      const api_url = "dash_get_customer";
      const method = "POST";

      const response = await getHeaders(requestData, api_url, method);

      const data = await response?.json();

      const responseData = data?.result?.map(([name, value]) => ({
        name,
        value,
        fill: `#${Math.floor(Math.random() * 16777215).toString(16)}`,
      }));

      const dataArray = data?.result?.map((item) => {
        const [customer, sum] = item;
        return {
          Customer: customer,
          Sum: sum,
        };
      });

      set({
        chartData: responseData,
        customerData: dataArray,
        isLoading: false,
      });

      return { chartData: responseData, customerData: dataArray };
    } catch (error) {
      console.error("Error:", error);
      set({ isLoading: false });
    }
  },

  fetchProductData: async (entityValue, countryValue, filingValue) => {
    try {
      const requestData = {
        entity_id: entityValue,
        country_id: countryValue,
        filing_id: filingValue,
      };

      const api_url = "dash_get_output_unspsc";
      const method = "POST";

      const response = await getHeaders(requestData, api_url, method);

      const data = await response?.json();

      const convertedData = data?.result?.map((row) => {
        return {
          Item: row[0],
          Sum: row[2],
          ItemDesc: row[1],
        };
      });

      set({
        productData: convertedData,
        isLoading: false,
      });

      return { productData: convertedData };
    } catch (error) {
      console.error("Error:", error);
      set({ isLoading: false });
    }
  },

  fetchInvoiceData: async (entityValue, countryValue, filingValue) => {
    try {
      const requestData = {
        entity_id: entityValue,
        country_id: countryValue,
        filing_id: filingValue,
      };

      const api_url = "dash_top_ten_sales_purchase";
      const method = "POST";

      const response = await getHeaders(requestData, api_url, method);

      const data = await response?.json();

      const invoice = data?.[0];
      const convertedData = invoice?.result?.map((row) => {
        return {
          Customer: row[0],
          InvoiceNo: row[1],
          Sum: row[2],
        };
      });

      set({
        invoiceData: convertedData,
        isLoading: false,
      });

      return { invoiceData: convertedData };
    } catch (error) {
      console.error("Error:", error);
      set({ isLoading: false });
    }
  },

  fetchAll: async (entityValue, countryValue, filingValue) => {
    try {
      const [customerDataPromise, productDataPromise, invoiceDataPromise] =
        await Promise.all([
          get().fetchCustomerData(entityValue, countryValue, filingValue),
          get().fetchProductData(entityValue, countryValue, filingValue),
          get().fetchInvoiceData(entityValue, countryValue, filingValue),
        ]);

      const [customerDataResult, productDataResult, invoiceDataResult] =
        await Promise.all([
          customerDataPromise,
          productDataPromise,
          invoiceDataPromise,
        ]);

      set({
        customerData: customerDataResult?.customerData,
        productData: productDataResult?.productData,
        invoiceData: invoiceDataResult?.invoiceData,
        isLoading: false,
      });
    } catch (error) {
      console.error("Error:", error);
      set({ isLoading: false });
    }
  },

  handleLogout: () => {
    set({
      chartData: null,
      invoiceData: null,
      productData: null,
      customerData: null,
      isLoading: true,
      isAuthenticated: false,
    });
  },

  handleLogin: () => {
    set({
      isAuthenticated: true,
    });
  },
}));

export default useApiStore;
