import { CssBaseline } from "@mui/material";
import React from "react";
import { AppAuth } from "./App.auth";
import { AppTheme } from "./App.theme";
import RouterApp from "./router";

const App = () => {
  return (
    <AppAuth>
      <AppTheme>
        <CssBaseline />
        <RouterApp />
      </AppTheme>
    </AppAuth>
  );
};
export default App;
